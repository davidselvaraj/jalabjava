package urma.behaviorParam2;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Created by Adam on 7/15/2015.
 */
public class UsingFilters {
    public static void main(String[] args) {

        List<String> myList =   Arrays.asList("a1", "a2", "b1", "c2", "c1");

        Predicate<String> startsWithC =  s -> s.startsWith("c");

        Function<String,String> capMe = s -> s.toUpperCase();

        Consumer<String> consumeMe = s -> System.out.println(s);


        //predicates are typically used in filters
        //functions are typically used in maps
        //consumers are used with terminal operations


        for (String s : myList) {
            System.out.println(s);
        }

        for (int i = 0; i < myList.size(); i++) {
            System.out.println(myList.get(i));
        }



        myList.stream()
                .filter(startsWithC) //intermediate
                .map(capMe) //intermediate
                .forEach(consumeMe); //terminal
    }
}
